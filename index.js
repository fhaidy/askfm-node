const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const connection = require("./database/database");
const ModelQuestion = require("./database/Question");
const ModelAnswer = require("./database/Answer");

app.set('view engine', 'ejs');
app.use(express.static('public'));

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get("/", (req, res) => {
    ModelQuestion.findAll({raw:true, order : [
        ['id', 'desc']
    ]}).then(questions =>{
        res.render('index',{
            questions: questions
        });
    });
});

app.get("/ask", (req, res) => {
    res.render('ask');
});

app.get("/question/:id", (req, res) => {
    let id = req.params.id;
    ModelQuestion.findOne({
        where: {id: id}
    }).then(question =>{
        if(question != undefined){
            ModelAnswer.findAll({
                where: {questionId: id},
                order:[['id', 'desc']]
            }).then(answers =>{
                res.render('question', {
                    question: question,
                    answers: answers
                });
            });
            
        }else{
            res.redirect("/");
        }
    });
});

app.post("/postQuestion", (req, res) => {
    ModelQuestion.create({
        title: req.body.title,
        description: req.body.description
    }).then(()=>{
        res.redirect("/");
    });
});

app.post("/postAnswer", (req, res) => {
    let questionId = req.body.questionId;
    ModelAnswer.create({
        body: req.body.body,
        questionId: questionId
    }).then(()=>{
        res.redirect("/question/"+questionId);
    });
});

app.listen(8080, () => {
    console.log("app rodando");
});